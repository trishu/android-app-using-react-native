# ToDoApp
- [x]  Adding new todo
- [x]  delete the completed todos

## Using
- React native
- Expo

# Install
<a href="https://gitlab.com/trishu/android-app-using-react-native/-/blob/master/todoapp/Apk/myproject.apk"><img src="ScreenShots/install-png-5.png" height="130" width="130"></a>

## ScreenShots

<img src="ScreenShots/splash-screen.jpeg" width="200">
<img src="ScreenShots/home-screen.jpeg" width="200" >
<img src="ScreenShots/added-todo.jpeg"  width="200">



